import React from "react";
import Table from "../../../components/Table";
import Select from "../../../components/Select";

import product1 from "../../../assets/images/products/1.jpeg";
import product2 from "../../../assets/images/products/2.jpeg";
import product3 from "../../../assets/images/products/3.jpeg";
import product4 from "../../../assets/images/products/4.jpeg";
import product5 from "../../../assets/images/products/5.jpeg";

const mappingStatusType = (status) => {
  return (
    {
      Active: "success",
      Deactive: "warning",
    }[status] || "secondary"
  );
};

const columns = [
  {
    title: "#",
    key: "imageUrl",
    render: (value, item, index) => (
      <div className="h-16 w-16">
        <img alt="" className="h-full w-full object-contain" src={item.imageUrl} />
      </div>
    ),
  },
  {
    title: "PRODUCT",
    key: "product",
    render: (value) => <a href="#">{value}</a>,
  },
  {
    title: "CATAGORY",
    key: "catagory",
  },
  {
    title: "ADDED DATE",
    key: "addedAt",
    render: (value) => <p className="text-sm">{value}</p>,
  },
  {
    title: "PRICE",
    key: "price",
  },
  {
    title: "QUANTITY",
    key: "quantity",
    render: (value, item) => (
      <div>
        <bullet className={item.tagType} /> {value}
      </div>
    ),
  },
  {
    title: "STATUS",
    key: "status",
    render: (value) => (
      <badge className={`${mappingStatusType(value)} light rounded-full`}>
        {value}
      </badge>
    ),
  },
  {
    title: "ACTION",
    key: "action",
    render: (value) => (
      <div className="flex flex-row">
        <i className="hover:text-gray-600 far fa-eye fa-lg pr-4 text-orange-400" />
        <i className="hover:text-gray-600 far fa-edit fa-lg pr-4 text-green-500" />
        <i className="hover:text-gray-600 far fa-trash-alt fa-lg pr-4 text-red-500" />
      </div>
    ),
  },
];

const data = [
  {
    imageUrl: product1,
    product: "Amazing Rolling Chair",
    catagory: "Office Chairs",
    addedAt: "16/12/2019",
    price: "$597.66",
    quantity: "654",
    status: "Active",
  },
  {
    imageUrl: product2,
    product: "Bluffly bag",
    catagory: "Kids cloths",
    addedAt: "06/13/2019",
    price: "$23",
    quantity: "758",
    status: "Active",
  },
  {
    imageUrl: product3,
    product: "Chamiu lipstick",
    catagory: "Woman cloath",
    addedAt: "09/05/2019",
    price: "$2.24",
    quantity: "541",
    status: "Active",
  },
  {
    imageUrl: product4,
    product: "Fostroad watch",
    catagory: "Man watch",
    addedAt: "03/24/2019",
    price: "$5",
    quantity: "204",
    status: "Deactive",
  },
  {
    imageUrl: product5,
    product: "Foxy Bag",
    catagory: "Woman cloths",
    addedAt: "06/30/2018",
    price: "$76",
    quantity: "560",
    status: "Deactive",
  },
  {
    imageUrl: product1,
    product: "Amazing Rolling Chair",
    catagory: "Office Chairs",
    addedAt: "16/12/2019",
    price: "$597.66",
    quantity: "654",
    status: "Active",
  },
  {
    imageUrl: product2,
    product: "Bluffly bag",
    catagory: "Kids cloths",
    addedAt: "06/13/2019",
    price: "$23",
    quantity: "758",
    status: "Active",
  },
  {
    imageUrl: product3,
    product: "Chamiu lipstick",
    catagory: "Woman cloath",
    addedAt: "09/05/2019",
    price: "$2.24",
    quantity: "541",
    status: "Active",
  },
  {
    imageUrl: product4,
    product: "Fostroad watch",
    catagory: "Man watch",
    addedAt: "03/24/2019",
    price: "$5",
    quantity: "204",
    status: "Deactive",
  },
  {
    imageUrl: product5,
    product: "Foxy Bag",
    catagory: "Woman cloths",
    addedAt: "06/30/2018",
    price: "$76",
    quantity: "560",
    status: "Deactive",
  },
];

const Ecommerce = (props) => {
  return (
    <grid>
      <div className="col-span-12">
        <div className="flex justify-end"></div>
        <card className="p-0">
          <div className="p-3 flex flex-row">
            <div className="left-icon w-full mr-2">
              <i class="fas fa-search text-indigo-500"></i>
              <input type="text" placeholder="search" />
            </div>
            <Select className="w-56 outline mr-2">
              <option>Filter</option>
              <option>Sent</option>
              <option>Downloaded</option>
              <option>Partial Payment</option>
              <option>Paid</option>
            </Select>
            <button className="primary">Create Product</button>
          </div>
          <div className="w-full overflow-scroll">
            <Table columns={columns} dataSource={data} />
          </div>
        </card>
      </div>
    </grid>
  );
};

export default Ecommerce;
