import React from "react";
import product from '../../../assets/images/products/product.jpg'
import Table from '../../../components/Table'
import ProgressBar from '../../../components/Progress/Bar'

const ProductDetail = props => {
  return <card className="md:p-8">
    <grid>
      <div className="col-span-12 md:col-span-6 p-2 md:p-12">
        <img src={product} alt="" className="w-full"/>
      </div>
      <div className="col-span-12 md:col-span-6 p-2">
        <sup>Jolyecart logistic</sup>
        <h5>Women Purple Sling Bag</h5>
        <br/>
        <p>Added Date: 09/12/2018</p>
        <br/>
        <badge className="success">Instock</badge>
        <b> Special price:</b>
        <br/>
        <br/>
        <span><h4>$139.58</h4><span className="line-through">$322.53</span><b className="text-green-500"> 86% off</b></span> 
        <br/>
        <br/>
        <badge className="success text-base">3.8 <i className="far fa-star" /></badge> <span><b>179 ratings and 43 reviews</b></span>
        <br/>
        <br/>
        <p><i className="fa fa-tag text-green-500" /> <b>Bank Offer</b> 10% Instant Discount on ABCD Bank Credit & Debit Cards <span className="text-orange-500">T&C</span></p>
        <p><i className="fa fa-tag text-green-500" /> <b>Bank Offer</b> 5% Unlimited Cashback on Flipkart PQR Bank Credit Card <span className="text-orange-500">T&C</span></p>
        <p><i className="fa fa-tag text-green-500" /> <b>Bank Offer</b> Extra 5% off* with XYZ Bank Buzz Credit Card <span className="text-orange-500">T&C</span></p>
        <br/>
        <grid>
          <div className="col-span-12 md:col-span-6">
            <b>Color</b>
            <br/>
            <br/>
            <div className="flex flex-row">
              <div className="cursor-pointer w-8 h-8 bg-red-400 mr-2 rounded shadow"></div>
              <div className="cursor-pointer w-8 h-8 bg-yellow-400 mr-2 rounded shadow"></div>
              <div className="cursor-pointer w-8 h-8 bg-blue-400 mr-2 rounded shadow border border-black"></div>
              <div className="cursor-pointer w-8 h-8 bg-gray-800 mr-2 rounded shadow"></div>
            </div>
          </div>
          <div className="col-span-12 md:col-span-6">
            <b>Size</b>
            <br/>
            <br/>
            <badge className="cursor-pointer secondary outline">XS</badge>
            <badge className="cursor-pointer secondary outline">S</badge>
            <badge className="cursor-pointer info outline">M</badge>
            <badge className="cursor-pointer secondary outline">L</badge>
            <badge className="cursor-pointer secondary outline">XL</badge>
          </div>
          <div className="col-span-12 md:col-span-6">
            <button className="w-full warning"><i className="fa fa-shopping-cart" /> ADD TO CART</button>
          </div>
          <div className="col-span-12 md:col-span-6">
            <button className="w-full success"><i className="fa fa-bolt" /> BUY NOW</button>
          </div>
        </grid>
      </div>
      <div className="col-span-12">
        <b>Description:</b>
        <br/>
        <p>
          This stylish Sling Bag SAHAL FASHION from is best to carry to work to add an element of style to your office wear.Made from premium. It is compact to store your important things with ease.Made From outer material PU and inner material From Fabric. Furthermore, they feature Turnlock closure are best suited for a casual and partywear occasion.
        </p>
        <br/>
        <table>
          <tr>
            <td className="pr-12">Material</td>
            <td>PU</td>
          </tr>
          <tr>
            <td className="pr-12">Number of Compartments</td>
            <td>2</td>
          </tr>
          <tr>
            <td className="pr-12">Number of Compartments</td>
            <td>2</td>
          </tr>
          <tr>
            <td className="pr-12">Model Name</td>
            <td>Sahalsling bag</td>
          </tr>
          <tr>
            <td className="pr-12">Bag Size</td>
            <td>Regular</td>
          </tr>
          <tr>
            <td className="pr-12">Closure</td>
            <td>Magnetic Button</td>
          </tr>
          <tr>
            <td className="pr-12">Pattern</td>
            <td>Solid</td>
          </tr>
          <tr>
            <td className="pr-12">Pack of</td>
            <td>1</td>
          </tr>
          <tr>
            <td className="pr-12">Sales Package</td>
            <td>1 sling bag</td>
          </tr>
          <tr>
            <td className="pr-12">Width</td>
            <td>10 mm</td>
          </tr>
          <tr>
            <td className="pr-12">Height</td>
            <td>27 mm</td>
          </tr>
          <tr>
            <td className="pr-12">Weight</td>
            <td>250 g</td>
          </tr>
        </table>
        <br/>
        <b className="text-indigo-500">Manufacturing, Packaging and Import Info</b>
      </div>
      <div className="col-span-12">
        <Table
          columns={[
            {
              title: "Seller",
              key: "seller"
            },
            {
              title: "Price",
              key: "price",
              render: text => (
                <div>
                  <i class="fas fa-angle-double-up pr-2 text-green-500" />
                  {text}
                </div>
              )
            },
            {
              title: "Stock",
              key: "stock",
              render: (text) => <div>
                {text}
                <ProgressBar value={text / 1000 * 100} />
              </div>
            },
            {
              title: "Revenue",
              key: "revenue",
              render: text => <div className="text-green-500">{text}</div>
            }
          ]}
          dataSource={[
            {
              seller: "Octroyee mall USA",
              price: "$139.50",
              stock: "478",
              revenue: "$1,89,547"
            },
            {
              seller: "Chromin - Brazil",
              price: "$149.99",
              stock: "122",
              revenue: "$87,245"
            },
            {
              seller: "Milan - Navsari",
              price: "$106.87",
              stock: "781",
              revenue: "$5,87,478"
            },
            {
              seller: "Xendrasn - Japan",
              price: "$239.66",
              stock: "956",
              revenue: "$55,781"
            },
          ]}
        />
      </div>
    </grid>
  </card>;
};

export default ProductDetail;
