import React from "react";
import Select from "../../../components/Select";
import Switch from "../../../components/Switch";

const InvoiceEdit = props => {
  return <grid>
    <card className="col-span-9 p-4">
      <div className="flex items-center justify-between w-full">
        <div className="flex items-center">
          <label>Invoice#</label>
          <div className="w-48 ml-2">
            <input type="text" placeholder="#000" value="10000"/>
          </div>
        </div>
        <div className="flex items-center">
          <label>Date Issue: </label>
          <div className="w-48 ml-2 mr-2">
            <input type="text" placeholder="Select Date" value="2020/01/01"/>
          </div>
          <label>Date Due: </label>
          <div className="w-48 ml-2">
            <input type="text" placeholder="Select Date" value="2020/02/01"/>
          </div>
        </div>
      </div>

      <div className="flex flex-col">
        <br/><hr/><br/>

        <h6 className="text-indigo-500">Invoice</h6>
        <div className="w-full md:w-1/2">
          <input type="text" placeholder="Product name" value="Kittap - admin"/>
        </div>

        <br/><hr/><br/>

        <p>Bill to</p>
        <div className="w-full md:w-1/2">
          <input type="text" placeholder="House no." className="mb-2" value="Kittapard LTD."/>
          <textarea type="text" placeholder="Landmark/Street" className="mb-1" value="Bangkok Thailand">
          </textarea>
          <input type="text" placeholder="City" className="mb-2" value="Bangkok"/>
          <input type="text" placeholder="Pincode" value="10250"/>
        </div>

        <br/><hr/><br/>
        
        <div className="grid grid-cols-12 gap-8 px-4 mb-2">
          <div className="col-span-3">Item</div>
          <div className="col-span-3">Cost</div>
          <div className="col-span-3">Qty</div>
          <div className="col-span-3">Price</div>
        </div>
        <div className="grid grid-cols-12 gap-8 p-4 border rounded">
          <Select className="col-span-3">
            <option>Product A</option>
            <option>Product B</option>
            <option selected>Product C</option>
            <option>Product D</option>
          </Select>
          <input type="text" placeholder="0" className="col-span-3" value="10"/>
          <input type="text" placeholder="0" className="col-span-3" value="1"/>
          <div className="col-span-3">
            <b className="text-indigo-500">$10.00</b>
          </div>
          <input type="text" placeholder="Note." className="col-span-3" value="Best template"/>
          <div className="col-span-3">
            <p>Discount: 10% 10% 10%</p>
          </div>
        </div>
        <button className="mt-4 w-40 primary">
          <i className="fas fa-plus mr-2"></i>Add Item
        </button>

        <br/><hr/><br/>

        <grid>
          <div className="col-span-6">
            <input type="text" placeholder="Add Payment Terms" className="mb-4" value="Partial Payment"/>
            <input type="text" placeholder="All client Note" className="mb-4" value="Happy to give you a 10% discount."/>
          </div>
          <div className="col-span-6 px-8">
            <grid>
              <div className="col-span-6">
                <p>Subtotal</p>
                <p>Discount</p>
                <p>Tax</p>
                <p>Invoice Total</p>
                <p>Paid to date</p>
                <p>Balance (USD)</p>
              </div>
              <div className="col-span-6 text-right">
                <p>$00.00</p>
                <p>- $ 00.00</p>
                <p>0.0%</p>
                <p>$ 00.00</p>
                <p>- $ 00.00</p>
                <p>$ 000</p>
              </div>
            </grid>
            <br/>
            <button className="primary w-full">Preview</button>
          </div>
        </grid>

      </div>
    </card>

    <div className="col-span-3">
      <card className="p-4 h-auto">
        <grid className="gap-2 w-full">
          <div className="col-span-12">
            <button className="primary w-full">
              <i className="fas fa-paper-plane mr-2"/>Send Invoice
            </button>
          </div>
          <div className="col-span-12">
            <button className="w-full">Download Invoice</button>
          </div>
          <div className="col-span-6">
            <button className="w-full">Preview</button>
          </div>
          <div className="col-span-6">
            <button className="w-full">Save</button>
          </div>
          <div className="col-span-12">
            <button className="success w-full">Add Payment</button>
          </div>
        </grid>
      </card>
      <br/>
      <div>
        <p>Accept payments via</p>
        <Select className="w-full my-4 secondary outline">
          <option>Debit Card</option>
          <option>Credit Card</option>
          <option>Paypal</option>
          <option>Internet Banking</option>
          <option>UPI Transfer</option>
        </Select>

        <div className="flex justify-between items-center mb-2">
          <p>Payment Terms</p>
          <Switch name="paymentTerm" checked/>
        </div>
        <div className="flex justify-between items-center mb-2">
          <p>Client Notes</p>
          <Switch name="clientNotes"/>
        </div>
        <div className="flex justify-between items-center">
          <p>Payment Stub</p>
          <Switch name="paymentStub"/>
        </div>
      </div>
    </div>
  </grid>;
};

export default InvoiceEdit;
