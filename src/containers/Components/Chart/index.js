import React from "react";
import Doughnut from '../../../components/Charts/Doughnut'
import Bar from '../../../components/Charts/Bar'
import Line from '../../../components/Charts/Line'
import Radar from '../../../components/Charts/Radar'

const ComponentProgress = (props) => {
  return (
    <React.Fragment>
      <grid>
        <card className="col-span-12 md:col-span-6 lg:col-span-4 p-4">
          <h5>Doughnut</h5>
          <hr />
          <br />
          <Doughnut/>
          <br />
        </card>
        <card className="col-span-12 md:col-span-6 lg:col-span-4 p-4">
          <h5>Bar</h5>
          <hr />
          <br />
          <Bar/>
          <br />
        </card>
        <card className="col-span-12 md:col-span-6 lg:col-span-4 p-4">
          <h5>Line</h5>
          <hr />
          <br />
          <Line/>
          <br />
        </card>
        <card className="col-span-12 md:col-span-6 lg:col-span-4 p-4">
          <h5>Radar</h5>
          <hr />
          <br />
          <Radar/>
          <br />
        </card>
      </grid>
    </React.Fragment>
  );
};

export default ComponentProgress;
