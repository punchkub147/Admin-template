import React from "react";
import Switch from '../../../components/Switch'

const ComponentSwitch = (props) => {
  return (
    <React.Fragment>
      <grid>
        <card className="col-span-12 p-4">
          <h5>Basic Switch</h5>
          <hr />
          <br />
          <Switch name="basic"/>
          <br />
        </card>
        <card className="col-span-12 p-4">
          <h5>Checked Switch</h5>
          <hr />
          <br />
          <Switch name="checked" checked/>
          <br />
        </card>
        <card className="col-span-12 p-4">
          <h5>Disabled Switch</h5>
          <hr />
          <br />
          <Switch name="disabled" disabled/>
          <br />
        </card>
        </grid>
    </React.Fragment>
  );
};

export default ComponentSwitch;
