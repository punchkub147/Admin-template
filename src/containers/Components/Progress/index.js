import React from "react";
import ProgressBar from '../../../components/Progress/Bar'

const ComponentProgress = (props) => {
  return (
    <React.Fragment>
      <grid>
        <card className="col-span-12 p-4">
          <h5>Basic Progress bar</h5>
          <hr />
          <br />
          <div className="w-40">
            <p>Value: 0</p>
            <ProgressBar value="0"/>
            <br/>
            <p>Value: 25</p>
            <ProgressBar value="25"/>
            <br/>
            <p>Value: 50</p>
            <ProgressBar value="50"/>
            <br/>
            <p>Value: 70</p>
            <ProgressBar value="70"/>
            <br/>
            <p>Value: 90</p>
            <ProgressBar value="90"/>
            <br/>
            <p>Value: 100</p>
            <ProgressBar value="100"/>
          </div>
          <br />
        </card>
      </grid>
    </React.Fragment>
  );
};

export default ComponentProgress;
