import React from "react";

import TableBasic from "./Basic";
import TableAction from "./Action";

const ComponentTable = (props) => {
  return (
    <React.Fragment>
      <grid>
        <card className="col-span-12 p-4">
          <h5>Tables</h5>
          <hr />
          <br />
          <h6>Basic</h6>
          <br />

          <TableBasic />
        </card>
        <card className="col-span-12 p-4">
          <h6>Table action</h6>
          <br />

          <TableAction />
        </card>
      </grid>
    </React.Fragment>
  );
};

export default ComponentTable;
