import React from "react";
import Card from "../../components/Card";
import Table from "../../components/Table";
import ProgressBar from "../../components/Progress/Bar";

const random = (min, max) => Math.floor(Math.random() * (max - min) + min);

const imageAvatar =
  "https://avatars0.githubusercontent.com/u/810438?s=460&u=4e652785fe5aa4aae075c40815741e8721f11fd5&v=4";

export default (props) => {
  const data = Array(7)
    .fill({})
    .map((obj) => ({
      profile: "Kittapard Puaime",
      stat: random(10, 100),
      value: `$${random(100, 999)}`,
    }));
  return (
    <card className="p-0">
      <div className="p-4">
        <h6>Stat</h6>
      </div>
      <ul className="flex flex-col h-full overflow-scroll">
        {data.map((item) => (
          <li className="px-4">
            <div className="flex flex-row items-center w-full justify-between">
              <div className="flex items-center">
                <img
                  src={imageAvatar}
                  alt=""
                  className="w-8 h-8 rounded-full mr-4"
                />
                <p
                  className="text-sm m-0 w-24 truncate mr-4"
                  title={item.profile}
                >
                  {item.profile}
                </p>
                <div className="w-24">
                  <ProgressBar value={item.stat} />
                </div>
              </div>

              <div>{item.value}</div>
            </div>
          </li>
        ))}
      </ul>
    </card>
  );
};
