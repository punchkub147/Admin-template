import React from "react";

export default ({ className, name, ...props }) => {
  return (
    <label for={name}>
      <div class="switch">
        <input id={name} name={name} type="checkbox" {...props} />
        <div class="track"></div>
        <div class="thumb"></div>
      </div>
    </label>
  );
};
